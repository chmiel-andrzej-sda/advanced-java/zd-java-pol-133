package eu.andret.sdacademy.game;

import java.util.Objects;

public abstract class LivingEntity extends Entity implements Speakable {
	protected int health;
	protected int mana;
	private int level;
	private int gold;
	private Inventory inventory = new Inventory();
	private Item itemInHand;

	protected LivingEntity(String name, Location location, int health, int mana, int level, int gold, Item itemInHand) {
		super(name, location);
		this.health = health;
		this.mana = mana;
		this.level = level;
		this.gold = gold;
		this.itemInHand = itemInHand;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		if (health >= 0 && health <= 100) {
			this.health = health;
		}
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		if (mana >= 0 && mana <= 100) {
			this.mana = mana;
		}
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public Item getItemInHand() {
		return itemInHand;
	}

	public void setItemInHand(Item itemInHand) {
		this.itemInHand = itemInHand;
	}

	public abstract void speak(String text);

	@Override
	public String toString() {
		return "LivingEntity{" +
				"health=" + health +
				", mana=" + mana +
				", level=" + level +
				", gold=" + gold +
				", inventory=" + inventory +
				", itemInHand=" + itemInHand +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof LivingEntity that)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return health == that.health
				&& mana == that.mana
				&& level == that.level
				&& gold == that.gold
				&& Objects.equals(inventory, that.inventory)
				&& Objects.equals(itemInHand, that.itemInHand);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), health, mana, level, gold, inventory, itemInHand);
	}
}
