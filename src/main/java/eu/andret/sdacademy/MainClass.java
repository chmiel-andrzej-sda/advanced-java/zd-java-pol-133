package eu.andret.sdacademy;

import eu.andret.sdacademy.game.ArmorItem;
import eu.andret.sdacademy.game.Chestplate;
import eu.andret.sdacademy.game.CustomRuntimeException;
import eu.andret.sdacademy.game.Entity;
import eu.andret.sdacademy.game.Helmet;
import eu.andret.sdacademy.game.Inventory;
import eu.andret.sdacademy.game.Item;
import eu.andret.sdacademy.game.LivingEntity;
import eu.andret.sdacademy.game.Location;
import eu.andret.sdacademy.game.Material;
import eu.andret.sdacademy.game.Metadata;
import eu.andret.sdacademy.game.NPC;
import eu.andret.sdacademy.game.Player;
import eu.andret.sdacademy.game.Speakable;
import eu.andret.sdacademy.game.Sword;

import javax.lang.model.UnknownEntityException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MainClass {
	private static final Scanner SCANNER = new Scanner(System.in);
	private static final Random RANDOM = new Random();
	private static final List<Class<?>> CLASSES = List.of(
			byte.class,
			char.class,
			short.class,
			int.class,
			long.class,
			double.class,
			float.class
	);

	public static void main(String[] args) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException, InstantiationException {
		Address address = new Address("Colorize", "Krakow", "PL");

		Person person = new Student("Andrzej", "Chmiel", address, Gender.MALE, 1);
		Person person1 = new Teacher("Andrzej", "Chmiel", address, Gender.MALE, "math");

		person.work();

		System.out.println(person.getAddress());

		Student student = new Student("S1", "S1", null, Gender.FEMALE, 1);
		System.out.println(student.getSurname());
		Location location = new Location(0, 0);
		Entity player = new Player("Andret", location, 100, 100, 1, 0, null);

		Player player1 = new Player("Andret3", location, 100, 100, 1, 0, null);
		Player player2 = new Player("Andret2", location, 100, 100, 1, 0, null);
		Player player3 = new Player("Andret1", location, 100, 100, 1, 1, null);
		NPC npc = new NPC("Andret", location, 100, 100, 1, 0, null, "carpenter");

		LivingEntity[] entities = {player1, player2, player3, npc};

		System.out.println(Arrays.toString(entities));

		if (player instanceof final Player p1) {
			Item item1 = new Item("Pickaxe", "The pickaxe", 10);
			item1.setValue(10);
			p1.getInventory().add(item1);
			item1.add(Integer.class, new Metadata<>(1));
			List<Item> items = p1.getInventory().getItems();
			System.out.println(items.get(0).getValue());
			items.stream()
					.filter(Objects::nonNull)
					.map(Item::getMetadata)
					.flatMap(metadata -> metadata.entrySet().stream())
					.map(entry -> "Metadata: " + entry.getKey() + ": " + entry.getValue().getE())
					.forEach(System.out::println);
		}

		Walkable walkable = new Student("", "", null, Gender.FEMALE, 0);
		walkable.walk();

		player3.speak("x");

		System.out.println(person.getGender().getPl());

		Box<Long> integerBox = new Box<>(1L);
		System.out.println(integerBox.getE());

		go(player1);

		Person.Pet pet = new Person.Pet();

		class Test {
			private int x;
		}

		Test test = new Test();
		test.x = 10;

//		System.out.println(player1.getInventory().getItem(10).getName());
//		System.out.println(player1.getHelmet().getName());
//		System.out.println(player1.getInventory().getItem(-3));
		try {
			append("");
		} catch (StackOverflowError | NullPointerException error) {
			System.out.println("catched: " + error.getClass());
		} finally {
			System.out.println("finally");
		}

		try {
			throwException();
		} catch (CustomException e) {
			e.printStackTrace();
		}

		try {
			uncheckedException();
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
		try {
			checkedException();
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		try {
			uncheckedCustomException();
		} catch (CustomRuntimeException ex) {
			ex.printStackTrace();
		}

		try {
			checkedCustomException();
		} catch (CustomException e) {
			e.printStackTrace();
		}
//		System.out.println(readInt());
		List<Integer> x = new LinkedList<>();
		x.add(1);
		x.forEach(System.out::println);
		List<Integer> copy = new LinkedList<>(x);
		System.out.println(copy);

		Set<Person> set = new TreeSet<>((o1, o2) -> {
			if (o1.getSurname().equals(o2.getSurname())) {
				return o1.getName().compareTo(o2.getName());
			}
			return o1.getSurname().compareTo(o2.getSurname());
		});
		set.add(person1);
		set.add(student);
		System.out.println(set);

		Set<Entity> hashSet = new HashSet<>();
		Set<Entity> treeSet = new TreeSet<>((o1, o2) -> o1.getName().compareTo(o2.getName()));

		hashSet.add(player1);
		hashSet.add(player2);
		hashSet.add(player1);
		hashSet.add(player2);

		treeSet.add(player1);
		treeSet.add(player2);
		treeSet.add(player1);
		treeSet.add(player2);

		treeSet.stream()
				.map(entity -> "TreeSet: " + entity.getName())
				.forEach(System.out::println);

		hashSet.stream()
				.map(entity -> "HashSet: " + entity.getName())
				.forEach(System.out::println);

		List<Player> playerList = List.of(player1, player2, player1, player2);
		System.out.println(playerList.size());
		List<Player> players = new ArrayList<>(new HashSet<>(playerList));
		System.out.println(players.size());

		Map<Player, Integer> map = new HashMap<>();
		map.put(player2, 2);
		map.put(player1, 1);
		map.put(player2, 3);
		System.out.println(map.size());
		System.out.println(map.get(player2));
		map.remove(player2);
		System.out.println(map.get(player2));
		map.put(player2, null);
		System.out.println(map.get(player2));
		map.values()
				.forEach(System.out::println);
		map.entrySet()
				.stream()
				.map(entry -> entry.getKey().getName() + ", " + entry.getValue())
				.forEach(System.out::println);

		// Functional programming
		// creates something from nothing
		IntSupplier supplier = () -> 1;
		System.out.println(supplier.getAsInt());

		// creates nothing (consumes) from something
		Consumer<Person> consumer = person2 -> System.out.println("person: " + person2);
		consumer.accept(person1);

		// creates something from something
		ToIntFunction<Person> function = person3 -> person3.getSurname().length();
		System.out.println(function.applyAsInt(person1));

		// checks if something matches condition
		Predicate<Person> personPredicate = person4 -> person4.getGender().equals(Gender.MALE);
		System.out.println(personPredicate.test(person));

		IntUnaryOperator sum = integer -> {
			int result = 0;
			for (int i = 0; i < integer; i++) {
				result += i;
			}
			return result;
		};
		System.out.println(sum.applyAsInt(10));

		IntSupplier randomSupplier = () -> RANDOM.nextInt(10);
		System.out.println("random: " + randomSupplier.getAsInt());

		Consumer<String> stringConsumer = s -> {
			char[] chars = s.toCharArray();
			for (char aChar : chars) {
				System.out.println(aChar);
			}
		};
		stringConsumer.accept("abc");

		IntPredicate isPrime = number -> {
			if (number <= 1) {
				return false;
			}
			for (int i = 2; i < Math.sqrt(number); i++) {
				if (number % i == 0) {
					return false;
				}
			}
			return true;
		};
		System.out.println(isPrime.test(2));

		double streamSum = IntStream.of(1, 5, 9, 2, 6, 3, 11, -12, -13)
				.filter(e -> Math.abs(e) % 2 == 1)
				.peek(System.out::println)
				.map(integer -> integer * integer)
				.average()
				.orElse(0);
		System.out.println(streamSum);

		Stream.of(5, 9, 2, 8, 10, -2, 33)
				.filter(n -> n % 2 == 0)
				.forEach(System.out::println);

		// instead of Player pl = (Player) player;
		// we are getting rid of additional variable
		((Player) player).getInventory()
				.getItems()
				.stream()
				.map(Item::getName)
				.forEach(System.out::println);

		int sum1 = ((Player) player).getInventory()
				.getItems()
				.stream()
				.mapToInt(Item::getValue)
				.sum();
		System.out.println(sum1);

		((Player) player).getInventory()
				.getItems()
				.stream()
				.filter(item -> item.getMetadata().isEmpty())
				.forEach(System.out::println);

		players.stream()
				.filter(player4 -> player4.getHelmet() == null
						|| player4.getChestplate() == null
						|| player4.getLeggins() == null
						|| player4.getBoots() == null)
				.forEach(System.out::println);

		players.stream()
				.filter(player4 -> Stream.of(
								player4.getHelmet(),
								player4.getChestplate(),
								player4.getLeggins(),
								player4.getBoots())
						.anyMatch(Objects::isNull))
				.forEach(System.out::println);

		Optional.of(player2)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.forEach(System.out::println);

		Optional.of(player2)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.filter(items -> !items.isEmpty())
				.map(items -> items.get(0))
				.orElse(null);

		Material material = Optional.ofNullable(player2)
				.map(Player::getHelmet)
				.map(ArmorItem::getMaterial)
				.orElse(null);
		System.out.println(material);
		Optional.ofNullable(player2)
				.map(Player::getHelmet)
				.map(ArmorItem::getMaterial)
				.ifPresentOrElse(
						System.out::println,
						() -> System.out.println("null"));
		Integer integer = Optional.of(player3)
				.map(LivingEntity::getGold)
				.filter(integer1 -> integer1 > 0)
				.orElseThrow(IllegalArgumentException::new);
		System.out.println(integer);

		double v = Optional.of(player3)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.mapToInt(Item::getValue)
				.average()
				.orElse(-1);
		System.out.println(v);

		int sum2 = Optional.of(player3)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.map(Item::getMetadata)
				.mapToInt(Map::size)
				.sum();
		System.out.println(sum2);

		List<Item> items = Optional.of(player3)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.filter(it -> it.getMetadata().size() > 0)
				.toList();
		System.out.println(items);

		// java io
		File file = new File(".");
		File[] files = file.listFiles();
		System.out.println(Arrays.toString(files));
		System.out.println(file.getAbsolutePath());

		// java nio
		Path path = Paths.get(".", ".gitignore");
		List<String> strings = Files.readAllLines(path);
		System.out.println(strings);
		try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile())))) {
			System.out.println(bf.readLine());
		} finally {
			System.out.println("finally");
		}
		Path path1 = Paths.get("test", "test1", "test2");
		Files.createDirectories(path1);
		File file1 = Paths.get("test", "test1", "test2", "file.txt").toFile();
		try (PrintWriter printWriter = new PrintWriter(file1)) {
			printWriter.write("Hello!");
		}

		Player write1 = new Player(
				"Write1",
				new Location(2, 3),
				100,
				100,
				1,
				0,
				null);

		Player write2 = new Player(
				"Write2",
				new Location(0, 0),
				50,
				75,
				5,
				275,
				null);

		Player write3 = new Player(
				"Write3",
				new Location(-10, 10),
				1,
				1,
				10,
				1000,
				new Chestplate("Magical sword", "It's a sword", 100, 10, Material.IRON));
		writePlayer(write1);
		writePlayer(write2);
		writePlayer(write3);

		File pl = new File("players");
		File[] files1 = pl.listFiles();
		System.out.println(Arrays.toString(files1));

		System.out.println(readPlayer("Write1"));
		System.out.println(readPlayer("Write2"));
		System.out.println(readPlayer("Write3"));

		Thread thread = new Thread(() -> {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			System.out.println("Test");
		});
//		thread.start();

		Thread thread1 = new Thread(() -> {
			for (int i = 1; i <= 20; i++) {
				System.out.println(i);
			}
		});

		Thread thread2 = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				System.out.println(RANDOM.nextInt(100));
			}
		});

		Thread thread3 = new Thread(() -> {
			Scanner scanner = new Scanner(System.in);
			int number = Integer.parseInt(scanner.nextLine());
			int square = number * number;
			int cube = square * number;
			System.out.println(square + " " + cube);
		});

//		thread1.start();
//		thread2.start();
//		thread3.start();

		Thread numbersThread1 = new Printing(500);
		Thread numbersThread2 = new Printing(400);
		Thread numbersThread3 = new Printing(600);

//		numbersThread1.start();
//		numbersThread2.start();
//		numbersThread3.start();

		Resource resource = new Resource();
		new Thread(() -> {
			try {
				resource.setX(10);
				System.out.println(resource.getX());
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});
//				.start();

		new Thread(() -> {
			try {
				resource.setX(20);
				System.out.println(resource.getX());
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});
//				.start();

		IntResource intResource = new IntResource();
		Thread addingThread = new Thread(() -> {
			for (int i = 1; i <= 10; i++) {
				try {
					intResource.add(i);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});
//		addingThread.start();

		Thread printingThread = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				try {
					System.out.println(intResource.getIntegers());
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});
//		printingThread.start();

		ExecutorService executorService = Executors.newFixedThreadPool(10);
//		Future<Integer> hello = executorService.submit(() -> {
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				Thread.currentThread().start();
//			}
//			System.out.println("Hello");
//			return 1;
//		});
//		System.out.println(hello.get());

		Class<Person> personClass = Person.class;
		Method[] methods = personClass.getDeclaredMethods();
		Method method = methods[0];
		Method setAddress = personClass.getDeclaredMethod("setAddress", Address.class);
		Address address1 = new Address("", "", "");
		setAddress.setAccessible(true);
		setAddress.invoke(person1, address1);
		System.out.println(person1.getAddress());

		Class<ReflectionTest> reflectionTestClass = ReflectionTest.class;
		ReflectionTest reflectionTest = new ReflectionTest();
		Method[] declaredMethods = reflectionTestClass.getDeclaredMethods();
		Arrays.stream(declaredMethods)
				.filter(m -> !m.isAnnotationPresent(Ignore.class))
				.sorted((o1, o2) -> {
					int o1Priority = o1.isAnnotationPresent(Priority.class)
							? o1.getAnnotation(Priority.class).value()
							: 0;
					int o2Priority = o2.isAnnotationPresent(Priority.class)
							? o2.getAnnotation(Priority.class).value()
							: 0;
					return o2Priority - o1Priority;
				})
				.forEach(m -> {
					m.setAccessible(true);
					try {
						Object invoke = m.invoke(reflectionTest);
						if (m.isAnnotationPresent(Priority.class)) {
							System.out.println("desc = " + m.getAnnotation(Priority.class).description());
						}
						System.out.println(m.getReturnType() + ", " + invoke + ", " + m.getName());
					} catch (IllegalAccessException | InvocationTargetException e) {
						throw new RuntimeException(e);
					}
				});

		Field address2 = Person.class.getDeclaredField("address");
		address2.setAccessible(true);
		System.out.println(address2.get(person1));
		address2.set(person1, null);
		System.out.println(person1.getAddress());
		Helmet e = new Helmet("test", "test", 12, 3, Material.BRONZE);
		nullify(e);
		System.out.println(e);

		Method[] declaredMethods1 = Item.class.getDeclaredMethods();
		Arrays.stream(declaredMethods1)
				.filter(m -> m.isAnnotationPresent(Important.class))
				.forEach(m -> {
					Important annotation = m.getAnnotation(Important.class);
					System.out.println("m.getName() = " + m.getName());
					System.out.println("annotation.run() = " + annotation.run());
					System.out.println("annotation.value() = " + annotation.value());
					try {
						System.out.println(m.invoke(((Player) player).getInventory().getItems().get(0)));
					} catch (IllegalAccessException | InvocationTargetException ex) {
						throw new RuntimeException(ex);
					}
				});

//		ImmutableSample sample = new ImmutableSample(1, "test", items.get(0));
//		sample.item().setValue(12);
		Constructor<Sword> declaredConstructor = Sword.class
				.getDeclaredConstructor(String.class, String.class, int.class, Material.class);
		declaredConstructor.setAccessible(true);
		Sword sword = declaredConstructor.newInstance("Test1", "Test2", 2, Material.IRON);
		System.out.println(sword);

		Constructor<LivingEntity> declaredConstructor1 = LivingEntity.class
				.getDeclaredConstructor(String.class, Location.class, int.class, int.class, int.class, int.class, Item.class);
		declaredConstructor1.setAccessible(true);
//		LivingEntity livingEntity = declaredConstructor1.newInstance("Test1", null, 2, 3, 4, 5, sword);
//		System.out.println(livingEntity);

		Constructor<X> declaredConstructor2 = X.class
				.getDeclaredConstructor();
		declaredConstructor2.setAccessible(true);
		X xx = declaredConstructor2.newInstance();
		System.out.println(xx);
	}

	static <E> void nullify(E e) throws IllegalAccessException {
		Class<?> aClass = e.getClass();
		while (aClass != Object.class) {
			Field[] declaredFields = aClass.getDeclaredFields();
			for (Field declaredField : declaredFields) {
				declaredField.setAccessible(true);
				if (CLASSES.contains(declaredField.getType())) {
					declaredField.set(e, 0);
				} else if (boolean.class.equals(declaredField.getType())) {
					declaredField.set(e, false);
				} else {
					declaredField.set(e, null);
				}
			}
			aClass = aClass.getSuperclass();
		}
	}

	static class Printing extends Thread {
		private final int waitTime;

		public Printing(int waitTime) {
			this.waitTime = waitTime;
		}

		@Override
		public void run() {
			for (int i = 1; i <= 10; i++) {
				System.out.println(Thread.currentThread().getName() + ": " + i);
				try {
					Thread.sleep(waitTime);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	public static String readPlayer(String playerName) throws IOException {
		Path path = Paths.get("players", playerName + ".txt");
		List<String> strings = Files.readAllLines(path);
		return String.join("", strings);
	}

	public static void writePlayer(Player player) throws IOException {
		Path path1 = Paths.get("players");
		Files.createDirectories(path1);
		File file1 = Paths.get("players", player.getName() + ".txt").toFile();
		try (PrintWriter printWriter = new PrintWriter(file1)) {
			printWriter.write(player.toString());
		}
	}

	static void uncheckedException() {
		throw new IllegalArgumentException();
	}

	static void checkedException() throws ReflectiveOperationException {
		throw new ReflectiveOperationException();
	}

	static void uncheckedCustomException() {
		throw new CustomRuntimeException("");
	}

	static void checkedCustomException() throws CustomException {
		throw new CustomException("");
	}

	static String append(String text) {
		return text + append(text);
	}

	public static <E extends Person> void x(E e) {
		e.work();
	}

	public static <E extends Speakable> void go(E e) {
		e.speak();
	}

	public static void throwException() throws CustomException {
		throw new CustomException("Test");
	}

	public static int readInt() {
		while (true) {
			try {
				return SCANNER.nextInt();
			} catch (InputMismatchException ex) {
				SCANNER.nextLine();
				System.out.println("It's not a number");
			}
		}
	}
}

class Resource {
	private int x;

	public synchronized int getX() throws InterruptedException {
		Thread.sleep(2000);
		return x;
	}

	public synchronized void setX(int x) throws InterruptedException {
		Thread.sleep(2000);
		this.x = x;
	}
}

class X {
	private X() {
		throw new UnsupportedOperationException();
	}
}

enum Test11 {
	A {
		@Override
		void test() {

		}
	},
	B {
		@Override
		void test() {

		}
	},
	C {
		@Override
		void test() {

		}
	};

	private Test11() {

	}

	abstract void test();
}
