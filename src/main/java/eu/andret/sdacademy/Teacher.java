package eu.andret.sdacademy;

import java.util.Objects;

public non-sealed class Teacher extends Person {
	private final String subject;

	public Teacher(String name, String surname, Address address, Gender gender, String subject) {
		super(name, surname, address, gender);
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	@Override
	public void work() {
		System.out.println("I'm teaching");
	}

	@Override
	public String toString() {
		return "Teacher{" +
				"subject='" + subject + '\'' +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Teacher teacher)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return Objects.equals(subject, teacher.subject);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), subject);
	}

	@Override
	public void walk() {
		System.out.println("teacher is walking");
	}
}
