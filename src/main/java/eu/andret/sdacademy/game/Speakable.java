package eu.andret.sdacademy.game;

public interface Speakable {
	void speak();
}
