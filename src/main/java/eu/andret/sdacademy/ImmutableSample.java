package eu.andret.sdacademy;

import eu.andret.sdacademy.game.Item;

public record ImmutableSample(int x, String name, Item item) {

}
