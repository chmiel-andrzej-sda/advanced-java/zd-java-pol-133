package eu.andret.sdacademy;

import java.util.ArrayList;
import java.util.List;

public class IntResource {
	private final List<Integer> integers = new ArrayList<>();

	public synchronized List<Integer> getIntegers() throws InterruptedException {
		Thread.sleep(500);
		return integers;
	}

	public synchronized void add(int element) throws InterruptedException {
		Thread.sleep(500);
		integers.add(element);
	}
}
