package eu.andret.sdacademy.game;

import java.util.Objects;

public sealed class ArmorItem extends Item permits Boots, Chestplate, Leggins, Helmet {
	private int armorPoints;
	private Material material;
	private final Calculations calculations;

	private static class Calculations {
		private int realValue;

		public Calculations(int realValue) {
			this.realValue = realValue;
		}

		public int calculate(int armorPoints) {
			return (int) (this.realValue * armorPoints / 100.);
		}
	}

	public ArmorItem(String name, String description, int value, int armorPoints, Material material) {
		super(name, description, value);
		calculations = new Calculations(value);
		this.armorPoints = armorPoints;
		this.material = material;
	}

	public int getArmorPoints() {
		return armorPoints;
	}

	public void setArmorPoints(int armorPoints) {
		if (armorPoints >= 0 && armorPoints <= 100) {
			this.armorPoints = armorPoints;
			this.value = calculations.calculate(armorPoints);
		}
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@Override
	public void setValue(int value) {
		calculations.realValue = value;
		super.setValue(calculations.calculate(armorPoints));
	}

	@Override
	public String toString() {
		return "ArmorItem{" +
				"armorPoints=" + armorPoints +
				", material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ArmorItem armorItem)) {
			return false;
		}
		return armorPoints == armorItem.armorPoints
				&& Objects.equals(material, armorItem.material);
	}

	@Override
	public int hashCode() {
		return Objects.hash(armorPoints, material);
	}
}
