package eu.andret.sdacademy.game;

import java.util.Objects;

public class Metadata<E> {
	private E e;

	public Metadata(E e) {
		this.e = e;
	}

	public E getE() {
		return e;
	}

	public void setE(E e) {
		this.e = e;
	}

	@Override
	public String toString() {
		return "Metadata{" +
				"e=" + e +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Metadata<?> metadata)) {
			return false;
		}
		return Objects.equals(getE(), metadata.getE());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getE());
	}
}
