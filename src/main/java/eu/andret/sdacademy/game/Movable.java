package eu.andret.sdacademy.game;

public interface Movable {
	void setLocation(Location location);

	Location getLocation();
}
