package eu.andret.sdacademy.game;

public final class Helmet extends ArmorItem {
	public Helmet(String name, String description, int value, int armorPoints, Material material) {
		super(name, description, value, armorPoints, material);
	}

	@Override
	public String toString() {
		return "Helmet{} " + super.toString();
	}
}
