package eu.andret.sdacademy;

import java.util.Objects;

public final class Student extends Person {
	private final int indexNumber;

	public Student(String name, String surname, Address address, Gender gender, int indexNumber) {
		super(name, surname, address, gender);
		this.indexNumber = indexNumber;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	@Override
	public void work() {
		System.out.println("I'm learning");
	}

	@Override
	public String toString() {
		return "Student{" +
				"indexNumber=" + indexNumber +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Student student)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return indexNumber == student.indexNumber;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), indexNumber);
	}

	@Override
	public void walk() {
		System.out.println("Student is walking");
	}
}
