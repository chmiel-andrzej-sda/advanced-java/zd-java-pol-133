package eu.andret.sdacademy;

public enum Gender {
	MALE("Mężczyzna"),
	FEMALE("Kobieta");

	private final String pl;

	Gender(String pl) {
		this.pl = pl;
	}

	public String getPl() {
		return pl;
	}
}
