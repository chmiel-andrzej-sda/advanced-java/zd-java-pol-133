package eu.andret.sdacademy;

public class ReflectionTest {
	@Priority(value = 4, description = "public")
	public String publicTest() {
		return "public";
	}

	@Priority(description = "protected")
	protected String protectedTest() {
		return "protected";
	}

	@Priority(value = 2)
	String packagePrivateTest() {
		return "package-private";
	}

	@Ignore
	private String privateTest() {
		return "private";
	}
}
