package eu.andret.sdacademy.game;

import java.util.Objects;

public abstract class Entity implements Movable {
	private String name;
	private Location location;

	protected Entity(String name, Location location) {
		this.name = name;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public void setLocation(Location location) {
		if (location != null) {
			this.location = location;
		}
	}

	@Override
	public String toString() {
		return "Entity{" +
				"name='" + name + '\'' +
				", location=" + location +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Entity entity)) {
			return false;
		}
		return Objects.equals(name, entity.name)
				&& Objects.equals(location, entity.location);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, location);
	}
}
