package eu.andret.sdacademy;

import java.util.Objects;

public class Address {
	private String street;
	private String city;
	private String country;

	public Address(String street, String city, String country) {
		this.street = street;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		if (country != null) {
			this.country = country;
		}
	}

	@Override
	public String toString() {
		return "Address{" +
				"street='" + street + '\'' +
				", city='" + city + '\'' +
				", country='" + country + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Address address)) {
			return false;
		}
		return Objects.equals(street, address.street) && Objects.equals(city, address.city) && Objects.equals(country, address.country);
	}

	@Override
	public int hashCode() {
		return Objects.hash(street, city, country);
	}
}
