package eu.andret.sdacademy.game;

import eu.andret.sdacademy.Important;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public sealed class Item permits ArmorItem, Sword {
	protected String name;
	private String description;
	protected int value;
	private final Map<Class<?>, Metadata<?>> metadata = new HashMap<>();

	public Item(String name, String description, int value) {
		this.name = name;
		this.description = description;
		this.value = value;
	}

	public Item(Item other) {
		this.name = other.name;
		this.description = other.description;
		this.value = other.value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Important("test")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Important(value = "value", run = "test")
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Map<Class<?>, Metadata<?>> getMetadata() {
		return new HashMap<>(metadata);
	}

	public <T> void add(Class<T> clazz, Metadata<T> metadata) {
		this.metadata.put(clazz, metadata);
	}

	public <T> Metadata<T> get(Class<T> clazz) {
		return (Metadata<T>) metadata.get(clazz);
	}

	public <T> void remove(Class<T> clazz, Metadata<T> metadata) {
		this.metadata.remove(clazz, metadata);
	}

	@Override
	public String toString() {
		return "Item{" +
				"name='" + name + '\'' +
				", description='" + description + '\'' +
				", value=" + value +
				", metadata=" + metadata +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Item item)) {
			return false;
		}
		return value == item.value
				&& Objects.equals(name, item.name)
				&& Objects.equals(description, item.description)
				&& Objects.equals(metadata, item.metadata);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, description, value, metadata);
	}
}
