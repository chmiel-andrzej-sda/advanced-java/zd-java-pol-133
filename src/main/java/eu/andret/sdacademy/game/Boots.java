package eu.andret.sdacademy.game;

public final class Boots extends ArmorItem {
	public Boots(String name, String description, int value, int armorPoints, Material material) {
		super(name, description, value, armorPoints, material);
	}

	@Override
	public String toString() {
		return "Boots{} " + super.toString();
	}
}
