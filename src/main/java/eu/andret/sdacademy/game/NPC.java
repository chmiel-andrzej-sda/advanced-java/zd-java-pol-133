package eu.andret.sdacademy.game;

import java.util.Objects;

public class NPC extends LivingEntity {
	private final String role;

	public NPC(String name, Location location, int health, int mana, int level, int gold, Item itemInHand, String role) {
		super(name, location, health, mana, level, gold, itemInHand);
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	@Override
	public void speak(String text) {
		System.out.println(text);
	}

	@Override
	public String toString() {
		return "NPC{" +
				"role='" + role + '\'' +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof NPC npc)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return Objects.equals(role, npc.role);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), role);
	}

	@Override
	public void speak() {
		System.out.println("NPC is speaking");
	}
}
