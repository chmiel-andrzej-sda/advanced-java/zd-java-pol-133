package eu.andret.sdacademy;

import java.util.Objects;

public class Box<E extends Number> {
	private E e;

	public Box(E e) {
		this.e = e;
	}

	public E getE() {
		return e;
	}

	public void setE(E e) {
		this.e = e;
	}

	@Override
	public String toString() {
		return "Box{" +
				"e=" + e +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Box<?> box)) {
			return false;
		}
		return Objects.equals(getE(), box.getE());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getE());
	}
}
