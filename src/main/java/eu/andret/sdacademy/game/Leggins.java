package eu.andret.sdacademy.game;

public final class Leggins extends ArmorItem {
	public Leggins(String name, String description, int value, int armorPoints, Material material) {
		super(name, description, value, armorPoints, material);
	}

	@Override
	public String toString() {
		return "Leggins{} " + super.toString();
	}
}
