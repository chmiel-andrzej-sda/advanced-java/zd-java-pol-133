package eu.andret.sdacademy.game;

public enum Material {
	IRON(0.3f, "żelazo"),
	SILVER(0.5f, "srebro"),
	BRONZE(0.6f, "brąz");

	private final float points;
	private final String name;

	Material(float points, String name) {
		this.points = points;
		this.name = name;
	}

	public float getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}
}
