package eu.andret.sdacademy;

import java.util.Objects;

public abstract sealed class Person implements Walkable permits Student, Teacher {
	private String name;
	private String surname;
	private Address address;
	private Gender gender;

	public static class Pet {
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	protected Person(String name, String surname, Address address, Gender gender) {
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Address getAddress() {
		return address;
	}

	private void setAddress(Address address) {
		this.address = address;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public abstract void work();

	@Override
	public String toString() {
		return "Person{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", address=" + address +
				", gender=" + gender +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Person person)) {
			return false;
		}
		return Objects.equals(name, person.name)
				&& Objects.equals(surname, person.surname)
				&& Objects.equals(address, person.address)
				&& Objects.equals(gender, person.gender);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, surname, address, gender);
	}
}
