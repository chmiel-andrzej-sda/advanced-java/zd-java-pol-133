package eu.andret.sdacademy.game;

import java.util.Random;

public class Animal extends LivingEntity {
	private static final Random RANDOM = new Random();

	public Animal(String name, Location location, int health, int mana, int level, int gold, Item itemInHand) {
		super(name, location, health, mana, level, gold, itemInHand);
	}

	@Override
	public void speak(String text) {
		int number = RANDOM.nextInt(3) + 2;
		for (int i = 0; i < number; i++) {
			System.out.println(text);
		}
	}

	@Override
	public void speak() {
		System.out.println("Animal is speaking");
	}
}
