package eu.andret.sdacademy.game;

import java.util.Objects;

public non-sealed class Sword extends Item {
	private final Material material;

	protected Sword(String name, String description, int value, Material material) {
		super(name, description, value);
		this.material = material;
	}

	public Material getMaterial() {
		return material;
	}

	@Override
	public String toString() {
		return "Sword{" +
				"material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Sword sword)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return material == sword.material;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), material);
	}
}
