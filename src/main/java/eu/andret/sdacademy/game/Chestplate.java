package eu.andret.sdacademy.game;

public final class Chestplate extends ArmorItem {
	public Chestplate(String name, String description, int value, int armorPoints, Material material) {
		super(name, description, value, armorPoints, material);
	}

	@Override
	public String toString() {
		return "Chestplate{} " + super.toString();
	}
}
