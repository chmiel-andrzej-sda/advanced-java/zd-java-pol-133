package eu.andret.sdacademy.game;

import java.util.Objects;

public class Player extends LivingEntity implements Jumpible {
	private Helmet helmet;
	private Chestplate chestplate;
	private Leggins leggins;
	private Boots boots;

	public Player(String name, Location location, int health, int mana, int level, int gold, Item itemInHand) {
		super(name, location, health, mana, level, gold, itemInHand);
	}

	public Helmet getHelmet() {
		return helmet;
	}

	public void setHelmet(Helmet helmet) {
		this.helmet = helmet;
	}

	public Chestplate getChestplate() {
		return chestplate;
	}

	public void setChestplate(Chestplate chestplate) {
		this.chestplate = chestplate;
	}

	public Leggins getLeggins() {
		return leggins;
	}

	public void setLeggins(Leggins leggins) {
		this.leggins = leggins;
	}

	public Boots getBoots() {
		return boots;
	}

	public void setBoots(Boots boots) {
		this.boots = boots;
	}

	@Override
	public void speak(String text) {
		System.out.println(getName() + ": " + text);
	}

	@Override
	public String toString() {
		return "Player{" +
				"helmet=" + helmet +
				", chestplate=" + chestplate +
				", leggins=" + leggins +
				", boots=" + boots +
				"} " + super.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Player player)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return Objects.equals(helmet, player.helmet)
				&& Objects.equals(chestplate, player.chestplate)
				&& Objects.equals(leggins, player.leggins)
				&& Objects.equals(boots, player.boots);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), helmet, chestplate, leggins, boots);
	}

	@Override
	public void speak() {
		System.out.println("Player is speaking");
	}

	@Override
	public void jump() {
		System.out.println(getName() + " is jumping");
	}
}
